FROM node:lts-alpine3.16

RUN npm install -g @nestjs/cli@9.0.0

WORKDIR /home/app-user

COPY . .

RUN npm install

EXPOSE 4010
