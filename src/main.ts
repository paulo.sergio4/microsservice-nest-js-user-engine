import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { UsersModule } from './users.module';

const logger = new Logger('Main')
const port = process.env.PORT

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(UsersModule, {
    transport: Transport.KAFKA,
		options: {
			client: {
				clientId: 'users',
				brokers: ['kafka:9092'],
			},
			/*consumer: {
				groupId: 'user-consumer',
				allowAutoTopicCreation: true
			}*/
		}
  });
  await app
    .listen()
    .then(() => logger.log(`[USER-ENGINE] is running on port ${port}`))
    .catch(err => logger.log(err));
}
bootstrap();


//172.23.195.78

//192.168.32.2:41290
