import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { DatabaseModule } from './database/database.module';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './database/users-schema';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
		ConfigModule.forRoot(),
		DatabaseModule,
		MongooseModule.forFeature([{ name: 'users', schema: UserSchema }])
	],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
