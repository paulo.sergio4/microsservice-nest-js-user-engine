import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDto } from './dto/users-dto';

@Injectable()
export class UsersService {
	constructor(@InjectModel('users') private readonly userModel: Model<UserDto>){}

	async create(user: UserDto): Promise<UserDto> {
		return this.userModel.create(user);
	}

	async findAll(): Promise<UserDto[]> {
		return this.userModel.find();
	}
}
