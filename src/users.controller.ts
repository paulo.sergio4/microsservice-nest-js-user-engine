import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { UserDto } from './dto/users-dto';
import { UsersService } from './users.service';

@Controller()
export class UsersController {
  constructor(private readonly userService: UsersService) {}

	private readonly logger = new Logger(UsersController.name)

  @MessagePattern('find-all-user')
  async listusers(): Promise<UserDto[]> {
    return await this.userService.findAll();
  }

	@MessagePattern('create-user')
	async create(@Payload() data: any): Promise<UserDto> {
		this.logger.log(JSON.stringify(data))

		return await this.userService.create(data)
	}
}
