import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
	imports: [
		MongooseModule.forRoot('mongodb://db:27017/microsservice?authSource=users')
	]
})
export class DatabaseModule {}
